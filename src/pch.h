#ifndef PCH_H
#define PCH_H

#ifdef WIN32
#include <windows.h>
#endif

#include <polyhook2/Detour/ADetour.hpp>
#include <polyhook2/Detour/x86Detour.hpp>
#include <polyhook2/Detour/x64Detour.hpp>
#include <polyhook2/Detour/NatDetour.hpp>
#include <polyhook2/ZydisDisassembler.hpp>

#endif
