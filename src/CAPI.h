#pragma once

#if _WIN32 || _WIN64
#if _WIN64
#define ENVIRONMENT64
#else
#define ENVIRONMENT32
#endif
#endif

#define DLLEXPORT extern "C" __declspec(dllexport) 

//
// natDetour C API
//
DLLEXPORT CDECL PLH::NatDetour* ph2_natDetour_create(uint64_t fnAddress, uint64_t fnCallback, uint64_t* userTrampVar);

//
// x86Detour C API
//

DLLEXPORT CDECL PLH::x86Detour* ph2_x86Detour_create(uint64_t fnAddress, uint64_t fnCallback, uint64_t* userTrampVar);
DLLEXPORT CDECL bool ph2_x86Detour_hook(PLH::x86Detour* self);
DLLEXPORT CDECL PLH::Mode ph2_x86Detour_getArchType(PLH::x86Detour* self);
DLLEXPORT CDECL void ph2_x86Detour_destructor(PLH::x86Detour* self);

//
// x64Detour C API
//
#ifdef ENVIRONMENT64
DLLEXPORT CDECL PLH::x64Detour* ph2_x64Detour_create(uint64_t fnAddress, uint64_t fnCallback, uint64_t* userTrampVar);
DLLEXPORT CDECL bool ph2_x64Detour_hook(PLH::x64Detour* self);
DLLEXPORT CDECL PLH::Mode ph2_x64Detour_getArchType(PLH::x64Detour* self);
DLLEXPORT CDECL PLH::x64Detour::detour_scheme_t ph2_x64Detour_getDetourScheme(PLH::x64Detour* self);
DLLEXPORT CDECL void ph2_x64Detour_setDetourScheme(PLH::x64Detour* self, PLH::x64Detour::detour_scheme_t scheme);
DLLEXPORT CDECL uint8_t ph2_x64Detour_getMinJmpSize();
DLLEXPORT CDECL const char* ph2_x64Detour_printDetourScheme(PLH::x64Detour::detour_scheme_t scheme) ;
DLLEXPORT CDECL void ph2_x64Detour_destructor(PLH::x64Detour* self);
#endif

//
// ADetour C API
//
/*
DLLEXPORT CDECL PLH::Detour* ph2_Detour_new(uint64_t fnAddress, uint64_t fnCallback, uint64_t* userTrampVar, PLH::Mode mode);
DLLEXPORT CDECL uint64_t ph2_Detour_getFnAddress(PLH::Detour* self);
DLLEXPORT CDECL uint64_t ph2_Detour_getFnCallback(PLH::Detour* self);
DLLEXPORT CDECL uint64_t* ph2_Detour_getUserTrampVar(PLH::Detour* self);
*/

DLLEXPORT CDECL bool ph2_Detour_unHook(PLH::Detour* self);
DLLEXPORT CDECL bool ph2_Detour_reHook(PLH::Detour* self);
DLLEXPORT CDECL PLH::Mode ph2_Detour_getArchType(PLH::Detour* self);
DLLEXPORT CDECL uint8_t ph2_Detour_getMaxDepth(PLH::Detour* self);
DLLEXPORT CDECL void ph2_Detour_setMaxDepth(PLH::Detour* self, uint8_t maxDepth);
DLLEXPORT CDECL PLH::HookType ph2_Detour_getType(PLH::Detour* self);
DLLEXPORT CDECL void ph2_Detour_setIsFollowCallOnFnAddress(PLH::Detour* self, bool value);
DLLEXPORT CDECL void ph2_Detour_destructor(PLH::Detour* self);

//
// IHook C API
//

DLLEXPORT CDECL bool ph2_IHook_hook(PLH::IHook* self);
DLLEXPORT CDECL bool ph2_IHook_unHook(PLH::IHook* self);
DLLEXPORT CDECL bool ph2_IHook_reHook(PLH::IHook* self);
DLLEXPORT CDECL bool ph2_IHook_isHooked(PLH::IHook* self);
DLLEXPORT CDECL PLH::HookType ph2_IHook_getType(PLH::IHook* self);
DLLEXPORT CDECL void ph2_IHook_setDebug(PLH::IHook* self, bool status);
DLLEXPORT CDECL bool ph2_IHook_setHooked(PLH::IHook* self, bool state);
DLLEXPORT CDECL void ph2_IHook_destructor(PLH::IHook* self);

//
// MemAccessor C API
//
DLLEXPORT CDECL bool ph2_MemAccessor_copy(PLH::MemAccessor* self, uint64_t dest, uint64_t src, uint64_t size);
DLLEXPORT CDECL PLH::ProtFlag ph2_MemAccessor_protect(PLH::MemAccessor* self, uint64_t dest, uint64_t size, PLH::ProtFlag newProtection, bool& status);
DLLEXPORT CDECL bool ph2_MemAccessor_safe_read(PLH::MemAccessor* self, uint64_t src, uint64_t dest, uint64_t size, size_t& read);
DLLEXPORT CDECL bool ph2_MemAccessor_safe_write(PLH::MemAccessor* self, uint64_t src, uint64_t dest, uint64_t size, size_t& written);
DLLEXPORT CDECL void ph2_MemAccessor_destructor(PLH::MemAccessor* self);