#include "pch.h"
#include "CAPI.h"

//
// NATDetour C API
// 

CDECL PLH::NatDetour* ph2_natDetour_create(uint64_t fnAddress, uint64_t fnCallback, uint64_t* userTrampVar) {
    PLH::NatDetour* detour = new PLH::NatDetour(fnAddress, fnCallback, userTrampVar);
    return detour;
}

//
// x86Detour C API
//

CDECL PLH::x86Detour* ph2_x86Detour_create(uint64_t fnAddress, uint64_t fnCallback, uint64_t* userTrampVar) {
    PLH::x86Detour* detour = new PLH::x86Detour(fnAddress, fnCallback, userTrampVar);
    return detour;
}

CDECL bool ph2_x86Detour_hook(PLH::x86Detour* self) {
    return self->hook();
}

CDECL PLH::Mode ph2_x86Detour_getArchType(PLH::x86Detour* self) {
    return self->getArchType();
}

CDECL void ph2_x86Detour_destructor(PLH::x86Detour* self) {
    self->~x86Detour();
}

//
// x64Detour C API
//
#ifdef ENVIRONMENT64
CDECL PLH::x64Detour* ph2_x64Detour_create(uint64_t fnAddress, uint64_t fnCallback, uint64_t* userTrampVar) {
    PLH::x64Detour* detour = new PLH::x64Detour(fnAddress, fnCallback, userTrampVar);
    return detour;
}

CDECL bool ph2_x64Detour_hook(PLH::x64Detour* self) {
    return self->hook();
}

CDECL PLH::Mode ph2_x64Detour_getArchType(PLH::x64Detour* self) {
    return self->getArchType();
}

CDECL PLH::x64Detour::detour_scheme_t ph2_x64Detour_getDetourScheme(PLH::x64Detour* self) {
    return self->getDetourScheme();
}

CDECL void ph2_x64Detour_setDetourScheme(PLH::x64Detour* self, PLH::x64Detour::detour_scheme_t scheme) {
    self->setDetourScheme(scheme);
}

CDECL uint8_t ph2_x64Detour_getMinJmpSize() {
    return PLH::x64Detour::getMinJmpSize();
}

CDECL const char* ph2_x64Detour_printDetourScheme(PLH::x64Detour::detour_scheme_t scheme) {
    return PLH::x64Detour::printDetourScheme(scheme);
}

CDECL void ph2_x64Detour_destructor(PLH::x64Detour* self) {
    self->~x64Detour();
}
#endif

//
// ADetour C API
//
/*
CDECL PLH::Detour* ph2_Detour_new(uint64_t fnAddress, uint64_t fnCallback, uint64_t* userTrampVar, PLH::Mode mode) {
    return new PLH::Detour(fnAddress, fnCallback, userTrampVar, mode); 
}
CDECL uint64_t ph2_Detour_getFnAddress(PLH::Detour* self) {
    return self->m_fnAddress;
}
CDECL uint64_t ph2_Detour_getFnCallback(PLH::Detour* self) {
    return self->m_fnCallback;
}
CDECL uint64_t* ph2_Detour_getUserTrampVar(PLH::Detour* self) {
    return self->m_userTrampVar;
}
*/

CDECL bool ph2_Detour_unHook(PLH::Detour* self) {
    return self->unHook();
}

CDECL bool ph2_Detour_reHook(PLH::Detour* self) {
    return self->reHook();
}

CDECL PLH::Mode ph2_Detour_getArchType(PLH::Detour* self) {
    return self->getArchType();
}

CDECL uint8_t ph2_Detour_getMaxDepth(PLH::Detour* self) {
    return self->getMaxDepth();
}

CDECL void ph2_Detour_setMaxDepth(PLH::Detour* self, uint8_t maxDepth) {
    self->setMaxDepth(maxDepth);
}

CDECL PLH::HookType ph2_Detour_getType(PLH::Detour* self) {
    return self->getType();
}

CDECL void ph2_Detour_setIsFollowCallOnFnAddress(PLH::Detour* self, bool value) {
    self->setIsFollowCallOnFnAddress(value);
}

CDECL void ph2_Detour_destructor(PLH::Detour* self) {
    self->~Detour();
}

//
// IHook C API
//

CDECL bool ph2_IHook_hook(PLH::IHook* self) {
    return self->hook();
}

CDECL bool ph2_IHook_unHook(PLH::IHook* self) {
    return self->unHook();
}

CDECL bool ph2_IHook_reHook(PLH::IHook* self) {
    return self->reHook();
}

CDECL bool ph2_IHook_isHooked(PLH::IHook* self) {
    return self->isHooked();
}

CDECL PLH::HookType ph2_IHook_getType(PLH::IHook* self) {
    return self->getType();
}

CDECL void ph2_IHook_setDebug(PLH::IHook* self, bool status) {
    self->setDebug(status);
}

CDECL bool ph2_IHook_setHooked(PLH::IHook* self, bool state) {
    return self->setHooked(state);
}

CDECL void ph2_IHook_destructor(PLH::IHook* self) {
    self->~IHook();
}

//
// MemAccessor C API
//
CDECL bool ph2_MemAccessor_copy(PLH::MemAccessor* self, uint64_t dest, uint64_t src, uint64_t size) {
    return self->mem_copy(dest, src, size);
}

CDECL PLH::ProtFlag ph2_MemAccessor_protect(PLH::MemAccessor* self, uint64_t dest, uint64_t size, PLH::ProtFlag newProtection, bool& status) {
    return self->mem_protect(dest, size, newProtection, status);
}

CDECL bool ph2_MemAccessor_safe_read(PLH::MemAccessor* self, uint64_t src, uint64_t dest, uint64_t size, size_t& read) {
    return self->safe_mem_read(src, dest, size, read);
}

CDECL bool ph2_MemAccessor_safe_write(PLH::MemAccessor* self, uint64_t src, uint64_t dest, uint64_t size, size_t& written) {
    return self->safe_mem_write(src, dest, size, written);
}

CDECL void ph2_MemAccessor_destructor(PLH::MemAccessor* self) {
    self->~MemAccessor();
}