# cpolyhook2


## What it is

This is a very small C API wrapper for PolyHook2

## API Preview
| CallConv | Function Name  | Arguments | Return Type |
|-|-|-|-|
| CDECL    | ph2_x86Detour_create          | uintptr_t fnAddress, uintptr_t fnCallback, uintptr_t* userTrampVar| PLH::x86Detour*|
| CDECL    | ph2_x86Detour_hook            | PLH::x86Detour* self| bool|
| CDECL    | ph2_x86Detour_getArchType     | PLH::x86Detour* self| PLH::Mode|
| CDECL    | ph2_x86Detour_destructor      | PLH::x86Detour* self | void|
| CDECL    | ph2_x64Detour_create          | uintptr_t fnAddress, uintptr_t fnCallback, uintptr_t* userTrampVar| PLH::x64Detour*|
| CDECL    | ph2_x64Detour_hook            | PLH::x64Detour* self| bool|
| CDECL    | ph2_x64Detour_getArchType     | PLH::x64Detour* self| PLH::Mode|
| CDECL    | ph2_x64Detour_destructor      | PLH::x64Detour* self | void|
| CDECL    | ph2_Detour_unHook             | PLH::Detour* self| bool|
| CDECL    | ph2_Detour_getArchType        | PLH::Detour* self| PLH::Mode|
| CDECL    | ph2_Detour_getMaxDepth        | PLH::Detour* self| uint8_t|
| CDECL    | ph2_Detour_setMaxDepth        | PLH::Detour* self, uint8_t maxDepth| void|
| CDECL    | ph2_Detour_getType            | PLH::Detour* self| PLH::HookType|
| CDECL    | ph2_Detour_setIsFollowCallOnFnAddress | PLH::Detour* self, bool value| void|
| CDECL    | ph2_Detour_destructor         | PLH::Detour* self| void|
| CDECL    | ph2_IHook_hook                | PLH::IHook* self| bool|
| CDECL    | ph2_IHook_unHook              | PLH::IHook* self| bool|
| CDECL    | ph2_IHook_reHook              | PLH::IHook* self| bool|
| CDECL    | ph2_IHook_isHooked           | PLH::IHook* self| bool  
| CDECL    | ph2_IHook_getType             | PLH::IHook* self| PLH::HookType
| CDECL    | ph2_IHook_setDebug            | PLH::IHook* self, bool status| void|
| CDECL    | ph2_IHook_setHooked           | PLH::IHook* self, bool state| bool|
| CDECL    | ph2_IHook_destructor          | PLH::IHook* self| void|
| CDECL    | ph2_MemAccessor_copy           | PLH::MemAccessor* self, uint64_t dest, uint64_t src, uint64_t size| bool|
| CDECL    | ph2_MemAccessor_protect       | PLH::MemAccessor* self, uint64_t dest, uint64_t size, PLH::ProtFlag newProtection, bool& status| PLH::ProtFlag
| CDECL    | ph2_MemAccessor_safe_read     | PLH::MemAccessor* self, uint64_t src, uint64_t dest, uint64_t size, size_t& read| bool|
| CDECL    | ph2_MemAccessor_safe_written  | PLH::MemAccessor* self, uint64_t src, uint64_t dest, uint64_t size, size_t& written| bool|
| CDECL    | ph2_MemAccessor_destructor    | PLH::MemAccessor* self| void

This table lists the function name, its calling convention (CallConv), arguments, and return type, formatted in Markdown for readability.

## Building

This project uses CMAKE and hence compiling should be easy as typing out
```
git clone https://gitlab.com/Rawra/cpolyhook2.git
cd cpolyhook2 && cmake .
```

It will generate valid vscproj/sln files for you to use.


#### Alternatively 

You can just clone and compile [PolyHook2](https://github.com/stevemk14ebr/PolyHook_2_0) as its a singular dependency.

You can use vcpkg to install it easily (x86 example):
```vcpkg install polyhook2:x86-windows```


## Credits

| Name           | Link                                                                         |
|----------------|------------------------------------------------------------------------------|
| PolyHook2| https://github.com/stevemk14ebr/PolyHook_2_0                                       |